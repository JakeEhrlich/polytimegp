package edu.kstate.kdd.macros

import scala.language.experimental.macros
import scala.reflect.macros.whitebox.Context

object ControlMacros {
  def tryNTimesFunc[T](n : Int)(f : => Option[T]): Option[T] = {
    for(i <- 0 until n) {
      f match {
        case None => {}
        case Some(x) => return Some(x)
      }
    }
    None
  }
  
  def tryNTimes[A](n : Int)(f : Option[A]): Option[A] = macro tryNTimesImp[A]

  def tryNTimesImp[A: c.WeakTypeTag](c: Context)(n : c.Expr[Int])(f : c.Expr[Option[A]]): c.Expr[Option[A]] = {
    import c.universe._
    c.Expr[Option[A]](q"""{
      val _n_ = $n
      if(_n_ != 0) {
        var _res_ = $f
        var _i_ = 1
        while(_i_ < _n_) {
          _i_ = _i_ + 1
          if(_res_.isEmpty) {
            _res_ = $f
          } else {
            _i_ = _n_
          }
        }
        _res_
      } else {
        None
      }
    }""")
  }
  
  def foldOver[A, B](z : B)(next : A, acc : B)(lst : List[A])(f : B): B = macro foldOverImpl[A, B]
  def foldOverImpl[A : c.WeakTypeTag, B : c.WeakTypeTag](c: Context)(z : c.Expr[B])(next : c.Expr[A], acc : c.Expr[B])(lst : c.Expr[List[A]])(f : c.Expr[B]): c.Expr[B] = {
    import c.universe._
    (next, acc) match {
      case (TermName(nextName), TermName(accName)) => {
        c.Expr[B](q"""{
          
        }""")
      }
      case otherwise => {
        throw new Exception("the 'next' and 'acc' varibles must be term names")
      }
    }
    //c.Expr[B](q"$z")
  }
  
  def printExpr(e : Any) = macro printExprImpl
  def printExprImpl(c : Context)(e : c.Expr[Any]) = {
    import c.universe._
    val s = c.Expr[String](Literal(Constant(e.toString())))
    c.Expr[Any](q"""println($s)""")
  }
}