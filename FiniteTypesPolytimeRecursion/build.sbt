name := "FiniteTypesPolytimeRecursion"

version := "1.0"

scalaVersion := "2.11.7"

scalaSource in Compile <<= (baseDirectory in Compile)(_ / "src")

libraryDependencies ++= Seq(
  "ch.qos.logback" % "logback-classic" % "1.1.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"
)
