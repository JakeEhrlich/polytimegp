package edu.kstate.kdd.polytime.SBella92

import scala.math.BigInt
import scala.math.BigDecimal
import scala.math.BigInt.int2bigInt
import edu.kstate.kdd.polytime._

/*
sealed abstract class Term extends Terms[Type] {
  val getType: Type
  def eval(normal : Term, safe : Term): Term = {
    this match {
      //case Fst =>
      //case Snd =>
      //case Safe =>
      //case Normal =>
      //case Cond =>
      case Rec(g, h0, h1) => {
        val normalEval = normal.eval(null, null)
        val PairTerm(NatConst(ns), x) = normalEval
        var n = ns
        var acc = g.eval(x, safe)
        var funcs = List[(BigInt, Term)]()
        while(n != 0) {
          val h = if((n & 1) == 1) h1 else h0
          n /= 2
          funcs = (n, h) :: funcs
        }
        for((y, f) <- funcs) acc = f.eval(PairTerm(NatConst(y), normalEval), PairTerm(safe, acc))
        acc
      }
      case Compose(h, r, t) => {
        h.eval(r.eval(normal, Unit()), t.eval(normal, safe)) 
      }
      //case Update
      case Unit() => Unit()
      case NullFunction(_, _) => Unit()
      //case FloatConst
      //case BinFunc
      //case UnaryFunc
      case NatConst(x) => NatConst(x)
      case Succ0() => safe.eval(null, null) match { case NatConst(x) => NatConst(2 * x)}
      case Succ1() => safe.eval(null, null) match { case NatConst(x) => NatConst(2 * x + 1)}
      case Pred() => safe.eval(null, null) match { case NatConst(x) => NatConst(x / 2)}
    }
  }
}

case class Unit() extends Term {
  val getType: Type = UnitType()
}

case class PairTerm(a : Term, b : Term) extends Term {
  val getType: Type = Pair(a.getType, b.getType)
}

//projection functions
case class Fst(n : Pair) extends Term {
  val getType: Type = {
    FuncType(UnitType(), n, n.pi1)
  }
}

case class Snd(n : Pair) extends Term {
  val getType: Type = {
    FuncType(n, UnitType(), n.pi2)
  }
}

case class Safe(n : Type, s : Type) extends Term {
  val getType: Type = {
    FuncType(n, s, s)
  }
}

case class Normal(n : Type, s : Type) extends Term {
  val getType: Type = {
    FuncType(n, s, n)
  }
}

//control functions
case class Cond(b : Type, c : Type) extends Term {
  val getType: Type = {
    FuncType(UnitType(), Pair(NatType(), Pair(b, c)), c)
  }
}
case class Rec(g : Term, h0 : Term, h1 : Term) extends Term {
  val getType: Type = {
    val FuncType(x, a, t) = g.getType
    FuncType(Pair(NatType(), x), a, t)
  }
}
case class Compose(h : Term, r : Term, t : Term) extends Term {
  val getType: Type = {
    //find all the types
    val FuncType(_, _, o) = h.getType
    val FuncType(_, s, _) = t.getType
    val FuncType(n, _, _) = r.getType 
    FuncType(n, s, o)
  }
}
case class Update(x : Type, y : Type, t : Term) extends Term {
  val getType: Type = {
    val FuncType(n, s, o) = t.getType
    FuncType(x, y, Pair(o, Pair(x, y)))
  }
}
case class NullFunction(s : Type, n : Type) extends Term {
  val getType: Type = {
    FuncType(s, n, UnitType())
  }
}

//floating point operations
case class FloatConst(value : BigDecimal) extends Term {
  val getType: Type = FloatType()
}
case class BinFunc(func : (BigDecimal, BigDecimal) => BigDecimal) extends Term {
  val getType: Type = FuncType(UnitType(), Pair(FloatType(), FloatType()), FloatType())
}
case class UnaryFunc(func : BigDecimal => BigDecimal) extends Term {
  val getType: Type = FuncType(UnitType(), FloatType(), FloatType())
}

//natural number operations
case class NatConst(value : BigInt) extends Term {
  val getType = NatType()
}
case class Succ0() extends Term {
  val getType = FuncType(UnitType(), NatType(), NatType())
}
case class Succ1() extends Term {
  val getType = FuncType(UnitType(), NatType(), NatType())
}
case class Pred() extends Term {
  val getType = FuncType(UnitType(), NatType(), NatType())
}
*/