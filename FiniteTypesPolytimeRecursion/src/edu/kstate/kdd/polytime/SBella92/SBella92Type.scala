package edu.kstate.kdd.polytime.SBella92

import edu.kstate.kdd.polytime._

sealed abstract class Type extends Types[Var, Type] with Unifiable[Var, Type] {
  def freeVars(): Set[Var] = {
    this match {
      case FuncType(n, s, o) => n.freeVars().union(s.freeVars().union(o.freeVars()))
      case Pair(p1, p2) => p1.freeVars().union(p2.freeVars())
      case NatType() => Set()
      case FloatType() => Set()
      case UnitType() => Set()
      case x : Var => Set(x)
    }
  }
  def apply(sub : Map[Var, Type]): Type = {
    this match {
      case FuncType(n, s, o) => FuncType(n(sub), s(sub), o(sub))
      case Pair(p1, p2) => Pair(p1(sub), p2(sub))
      case NatType() => NatType()
      case FloatType() => FloatType()
      case UnitType() => UnitType()
      case x : Var => if(sub.contains(x)) sub(x) else x
    }
  }
  def matchVar(x : Type): Option[Map[Var, Type]] = None
}

case class FuncType(normal : Type, safe : Type, out : Type) extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = {
    x match {
      case FuncType(n, s, o) => Some(List((n, normal), (s, safe), (o, out)))
      case _ => None
    }
  }
}
case class Pair(pi1 : Type, pi2 : Type) extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = {
    x match {
      case Pair(p1, p2) => Some(List((p1, pi1), (p2, pi2)))
      case _ => None
    }
  }
}
case class Var(name : String) extends Type {
  def matchesTop(x : Type) = Some(List())
  override def matchVar(x : Type): Option[Map[Var, Type]] = Some(Map() + (this -> x))
}
object Var {
  private val names = List("a", "b", "c", "d", "e", "t")
  private var iters = 0
  def getFreshVar(): Var = {
    val base = names(iters % names.length)
    iters += 1
    (iters - 1) / names.length match {
      case 0 => Var(base)
      case 1 => Var(base + "'")
      case 2 => Var(base + base)
      case x => Var(base + (x - 1))
    }
  }
}
case class NatType() extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = {
    x match {
      case NatType() => Some(List())
      case _ => None
    }
  }
}
case class FloatType() extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = {
    x match {
      case FloatType() => Some(List())
      case _ => None
    }
  }
}
case class UnitType() extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = {
    x match {
      case UnitType() => Some(List())
      case _ => None
    }
  }
}