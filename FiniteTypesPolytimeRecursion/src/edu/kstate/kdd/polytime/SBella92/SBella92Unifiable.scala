package edu.kstate.kdd.polytime.SBella92

import edu.kstate.kdd.polytime._

/*
//needed to give some top level terms to the whole package
object SBella92 {
  type UnifiableForm92 = UnifiableForm[Var, Type, Term]
  def unify(a : Type, b : Type): Either[Map[Var, Type], UnificationError[Type]] = {
    new Unificator[Var, Type]().unify(a, b)
  }
}

object FstUnifiable extends SBella92.UnifiableForm92 {
  private val av = Var.getFreshVar()
  val tipe = FuncType(UnitType(), Pair(av, Var.getFreshVar()), av)
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(_) => Some(List())
      case _ => None
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    (SBella92.unify(tipe, desiredType) : @unchecked) match {
      case Left(sub) => Fst(Pair(sub(Var("a")), sub(Var("b"))))
    }
  }
}

object SndUnifiable extends SBella92.UnifiableForm92 {
  private val av = Var.getFreshVar()
  private val bv = Var.getFreshVar()
  val tipe = FuncType(UnitType(), Pair(av, bv), bv)
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(_) => Some(List())
      case _ => None
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    (SBella92.unify(desiredType, tipe) : @unchecked) match {
      case Left(sub) => {
        val FuncType(_, out : Pair, _) = tipe(sub)
        Snd(out)
      }
    }
  }
}

object NullFuncUnifiable extends SBella92.UnifiableForm92 {
  private val av = Var.getFreshVar()
  private val bv = Var.getFreshVar()
  val tipe = FuncType(av, bv, UnitType())
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(_) => Some(List())
      case _ => None
    }
  }
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    (SBella92.unify(desiredType, tipe) : @unchecked) match {
      case Left(sub) => {
        val FuncType(s, n, _) = tipe(sub)
        NullFunction(s, n)
      }
    }
  }
}
/*
object SafeUnifiable extends SBella92.Unifiable {
  
}

object NormalUnifiable extends SBella92.Unifiable {
  
}

object CondUnifiable extends SBella92.Unifiable {
  
}
*/

object RecUnifiable extends SBella92.UnifiableForm92 {
  private val av = Var.getFreshVar()
  private val bv = Var.getFreshVar()
  private val cv = Var.getFreshVar()
  val tipe = FuncType(Pair(NatType(), av), bv, cv)
  private val gtipe = FuncType(av, bv, cv)
  private val h1tipe = FuncType(Pair(NatType(), av), Pair(bv, cv), cv)
  private val h2tipe = FuncType(Pair(NatType(), av), Pair(bv, cv), cv)
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(sub) => Some(List(gtipe(sub), h1tipe(sub), h2tipe(sub)))
      case _ => None
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    Rec(args(0), args(1), args(2))
  }
}

object ComposeUnifiable extends SBella92.UnifiableForm92 {
  override def toString() = "Compose"
  private val av = Var.getFreshVar()
  private val bv = Var.getFreshVar()
  private val cv = Var.getFreshVar()
  private val dv = Var.getFreshVar()
  private val ev = Var.getFreshVar()
  private val tipe = FuncType(av, bv, cv)
  private val htype = FuncType(dv, ev, cv)
  private val rtype = FuncType(av, UnitType(), dv)
  private val ttype = FuncType(av, bv, ev)
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(sub) => Some(List(htype(sub), rtype(sub), ttype(sub)))
      case _ => None
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    Compose(args(0), args(1), args(2))
  }
}

object Succ0Unifiable extends SBella92.UnifiableForm92 {
  override def toString() = "Succ0"
  val tipe = FuncType(UnitType(), NatType(), NatType())
    def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(_) => Some(List())
      case _ => None
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    Succ0()
  }
}

object Succ1Unifiable extends SBella92.UnifiableForm92 {
  override def toString() = "Succ1"
  val tipe = FuncType(UnitType(), NatType(), NatType())
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    SBella92.unify(tipe, desiredType) match {
      case Left(_) => Some(List())
      case _ => None
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    Succ1()
  }
}
*/