package edu.kstate.kdd.polytime

import scala.collection.parallel.mutable.ParArray
import scala.collection.mutable.ListBuffer

//mannages a run for genetic programing
//TODO: should store its current state every so often to a file so too much work is never lost
abstract class GPState[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit] {
  type Form = UnifiableForm[Var, Type, Term]
  
  //the actual population that is executed on
  private var pop : Array[Individual[Term, Fit]] = null
  def getPopulation() = pop.seq
  
  //this is the number of generations thus far
  private var iterations = 0
  def getGeneration() = iterations
  
  //explains how 1 new child should be created from the population
  val breeder: Breeder[Term, Fit]
  
  //defines how members of the initial population is created
  def genInitTerm(): Term
  
  //defines the size of the ininital population
  val populationSize: Int = 2000
  
  //we are also going to need a fast paralell collection of indecies
  private var parIndex : Array[Int] = null
  
  //defines how fitness is evaluated
  def getFitness(tm : Term): Fit
  
  //defines when you should stop
  def shouldStop(): Boolean
  
  //thougout the run we need to collect statistics of various sorts
  private var collectors = new ListBuffer[Collector[Var, Type, Term, Fit]]
  def addCollector(collector : Collector[Var, Type, Term, Fit]) {
    collectors += collector
  }
  
  //performs an inintilization that needs to occur first
  final def init() {
    parIndex = Array.tabulate(populationSize)(x => x)
    //produces the ininital population from a traversable
    pop = parIndex map (_ => Individual(genInitTerm(), getFitness))
    
  }
  
  //defines how we step from 1 population to the next
  protected final def step() {
    //tell collectors about the current state of things
    collectors map (_.collectBeforeStep(this))
    //now create the new generation
    pop = pop map (tm => breeder.gen(pop.seq))
    iterations = iterations + 1
    //tell collectors about the new state of things
    collectors map (_.collectAfterStep(this))
  }
  
  //defines the entire run (very simply as well)
  final def run() {
    init()
    while(!shouldStop()) {
      step()
    }
  }
}