package edu.kstate.kdd.polytime

import edu.kstate.kdd.polytime.ATAPLLinear._
import edu.kstate.kdd.polytime.ATAPLLinear.ATAPLLinear._
import org.slf4j.impl.StaticLoggerBinder

object Main {
  
  def main(args: Array[String]) {
    val binder = StaticLoggerBinder.getSingleton();
    System.out.println(binder.getLoggerFactory());
    System.out.println(binder.getLoggerFactoryClassStr());
    GpExample.run()
    return
  }
}