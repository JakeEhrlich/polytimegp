package edu.kstate.kdd.polytime.SBella99


abstract class Term
case class Lam(varName : String, tipe : Type, term : Term) extends Term
case class Comp(term : Term) extends Term
case class S0(term : Term) extends Term
