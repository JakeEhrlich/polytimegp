package edu.kstate.kdd.polytime.SBella99

abstract class Type {
  def ->(sigma : Type): Type = FuncType(this, sigma)
  def unary_!(): Type = CompType(this)
}
case class CompType(sigma : Type) extends Type
case class FuncType(t : Type, sigma : Type) extends Type
case class NatType() extends Type
case class DoubleType() extends Type