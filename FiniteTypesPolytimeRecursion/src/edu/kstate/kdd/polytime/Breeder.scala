package edu.kstate.kdd.polytime

import scala.collection.parallel.immutable._
//we need thread local random so things don't go wack
import java.util.concurrent.ThreadLocalRandom

//TODO: define crossover breeder, define mutation breeder
//TODO: defining the crossover breeder will make use of new methods that need to be added to Terms[T]
trait Breeder[T, Fit] {
  def gen(pop : Seq[Individual[T, Fit]]): Individual[T, Fit]
}

class TournamentSelection[Term, Fit](pipe : Breeder[Term, Fit], size : Int)(implicit ord : Ordering[Fit]) extends Breeder[Term, Fit] {
  def gen(pop : Seq[Individual[Term, Fit]]): Individual[Term, Fit] = {
    val terms = for(i <- 0 to size) yield pipe.gen(pop)
    (terms maxBy (_.fitness))
  }
}

class BestSelection[Term, Fit](implicit ord : Ordering[Fit]) extends Breeder[Term, Fit] {
  def gen(pop : Seq[Individual[Term, Fit]]): Individual[Term, Fit] = {
    (pop maxBy (_.fitness))
  }
}

class RandomSelection[Term, Fit](implicit ord : Ordering[Fit]) extends Breeder[Term, Fit] {
  def gen(pop : Seq[Individual[Term, Fit]]): Individual[Term, Fit] = {
    val idx = ThreadLocalRandom.current().nextInt(pop.size)
    pop(idx)
  }
}

//TODO: implement roullete wheel selection at some point
class FitPropSelection[Term, Fit](pipe : Breeder[Term, Fit])(implicit ord : Ordering[Fit], implicit val num : Numeric[Fit]) extends Breeder[Term, Fit] {
  def gen(pop : Seq[Individual[Term, Fit]]): Individual[Term, Fit] = {
    //first get the total fitness
    val totalFitness = num.toDouble(pop map (_.fitness) sum)
    
    //now go though each one and eventully select 1 but don't go too far
    for(_ <- 0 to pop.size) {
      val idv = pipe.gen(pop)
      val fit = num.toDouble(idv.fitness)
      if((fit / totalFitness) >= ThreadLocalRandom.current().nextDouble()) {
        return idv
      }
    }
    
    //some how (holy crap, how?) nothing was selected. so just pick something without bias
    pipe.gen(pop)
  }
}

//requests that you define the distrobution over depths
//the probability should be greater than or equal to 1 at max depth or less
abstract class Mutatator[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(val pipe : Breeder[Term, Fit], val prob : Double, val gen : Generator[Var, Type, Term], val maxDepth: Int, val maxWidth: Int) 
extends Breeder[Term, Fit] 
{
  
  //should be a distrobution from 0 to maxDepth (inclusive)
  def mutateHereDistro(depth : Int): Double
  
  //just randomlly selects (with uniform probability) an item out of a list
  private def select[T](items : List[T]): T = {
    items(ThreadLocalRandom.current().nextInt(items.size))
  }
  
  //traverses a new term and makes a new term out of it
  def makeNewTerm(t : Term, depth : Int, widthCredits : Int, tipe : Type): Term = {
    val parts = t.getParts
    if(parts.length == 0 || depth == maxDepth) {
      return t
    }
    if(mutateHereDistro(depth) >= ThreadLocalRandom.current().nextDouble()) {
      val hole = select(parts)
      val args = hole.adam.neededTermTypes(tipe) map (t => t map (ty => {
        (gen.generate(ty, maxDepth - depth, widthCredits))
      }))
      args match {
        case Some(lst) if lst.forall { x => x.isDefined } => hole.adam.makeTerm(tipe, lst map (_.get))
        case _ => t
      }
    } else {
      //traverse uniformlly to a random sub part
      val selected = select(parts)
      val newRib = makeNewTerm(selected.rib, depth + 1, maxWidth - t.width + selected.rib.width, selected.rib.getType)
      selected.eve(newRib) //all holes have 1 missing argument
    }
  }
  
  def gen(pop : Seq[Individual[Term, Fit]]): Individual[Term, Fit] = {
    //get a term from upstream
    val out = pipe.gen(pop)
    //dertermine if we even want to do this
    if(prob >= ThreadLocalRandom.current().nextDouble()) {
      Individual(makeNewTerm(out.term, 0, maxWidth, out.term.getType), out.getFit)
    } else {
      out
    }
  }
}

//the probability should be greater than or equal to 1 at max depth or less
abstract class CrossoverBreeder[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(val pipe : Breeder[Term, Fit], val prob : Double, val gen : Generator[Var, Type, Term], val maxDepth: Int, val maxWidth: Int) 
extends Breeder[Term, Fit]
{
   
}