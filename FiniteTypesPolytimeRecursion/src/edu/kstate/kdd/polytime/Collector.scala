package edu.kstate.kdd.polytime

import scala.collection.parallel.immutable._
import scala.collection.mutable.ListBuffer
import java.time._
import com.typesafe.scalalogging._
import org.slf4j.LoggerFactory

trait StringLogger {
  def log(s : String, args:AnyRef*)
}

//wraps objects (of any refrence type) 
class LazyStringable[T](obj : T) {
  override def toString(): String = obj.toString()
}

class StringLoggerTypeFactory(l : org.slf4j.Logger) {
  val logger = Logger(l)
  trait ErrorLogger extends StringLogger {
    override def log(s : String, args:AnyRef*) {
      logger.error(s, args : _*)
    }
  }
  
  //just does nothing
  trait FauxLogger extends StringLogger {
    override def log(s : String, args:AnyRef*) {}
  }
  
  //particular logging level
  trait DebugLogger extends StringLogger {
    override def log(s : String, args:AnyRef*) {
      logger.debug(s, args : _*)
    }
  }
  
  trait InfoLogger extends StringLogger {
    override def log(s : String, args:AnyRef*) {
      logger.info(s, args : _*)
    }
  }
  
}

//defines how we can collect statistics about many different things
trait Collector[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit] extends StringLogger {
  def collectBeforeStep(state : GPState[Var, Type, Term, Fit]) = {}
  def collectAfterStep(state : GPState[Var, Type, Term, Fit]) = {}
}

object TimeCollector {
  def apply[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
           (gpst : GPState[Var, Type, Term, Fit], sltf : StringLoggerTypeFactory)
           (implicit ord : Ordering[Fit]) = 
  {
    val out = new TimeCollector[Var, Type, Term, Fit]() with sltf.InfoLogger
    gpst.addCollector(out)
    out
  }
}

abstract class TimeCollector[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(implicit ord : Ordering[Fit]) extends Collector[Var, Type, Term, Fit]
{
  val start = Instant.now() //this a good way to get our first delta
  val times = ListBuffer[Instant]()
  override def collectAfterStep(state : GPState[Var, Type, Term, Fit]) = {
    val time = Instant.now()
    times += time
    log("time({}): {}", state.getGeneration() : java.lang.Integer, time)
  }
}

abstract class BestTermCollector[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(implicit ord : Ordering[Fit]) extends Collector[Var, Type, Term, Fit]
{
  private var bestOverall: Option[Individual[Term, Fit]] = None
  def getBestTerm(): Individual[Term, Fit] = {
    bestOverall.get
  }
  private val best = ListBuffer[Term]()
  
  //tells us who the best generation was at each step
  override def collectBeforeStep(state : GPState[Var, Type, Term, Fit]) = {
    val pop = state.getPopulation()
    val bestGen = pop.maxBy(_.fitness)
    bestOverall = bestOverall.orElse(Some(bestGen))
    best += bestGen.term
    bestOverall = bestOverall map { bestPre =>
      if(ord.gt(bestGen.fitness, bestPre.fitness)) bestGen
      else bestPre
    }
    log("bestTerm({}): {}", state.getGeneration() : java.lang.Integer, bestOverall.get.term)
  }
}

object BestTermCollector {
  def apply[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
           (gpst : GPState[Var, Type, Term, Fit], sltf : StringLoggerTypeFactory)
           (implicit ord : Ordering[Fit]) = 
  {
    val out = new BestTermCollector[Var, Type, Term, Fit]() with sltf.InfoLogger
    gpst.addCollector(out)
    out
  }
}

abstract class FitnessGraphCollector[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(implicit ord : Ordering[Fit]) extends Collector[Var, Type, Term, Fit]
{
  val fits = ListBuffer[Fit]()
  
  //tells us who the best fitness was at each step
  override def collectBeforeStep(state : GPState[Var, Type, Term, Fit]) = {
    val fit = (state.getPopulation() maxBy (_.fitness)).fitness
    fits += fit
    log("maxFit({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(fit))
  }
}

object FitnessGraphCollector {
  def apply[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
           (gpst : GPState[Var, Type, Term, Fit], sltf : StringLoggerTypeFactory)
           (implicit ord : Ordering[Fit]) = 
  {
    val out = new FitnessGraphCollector[Var, Type, Term, Fit]() with sltf.InfoLogger
    gpst.addCollector(out)
    out
  }
}

abstract class NumericalFitnessGraphCollector[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(implicit num : Numeric[Fit]) extends Collector[Var, Type, Term, Fit]
{
  val maxFits = ListBuffer[Fit]()
  val minFits = ListBuffer[Fit]()
  val avgFits = ListBuffer[Double]()
  val totalFits = ListBuffer[Fit]( )
  
  //tells us who the best fitness was at each step
  override def collectBeforeStep(state : GPState[Var, Type, Term, Fit]) = {
    val pop = state.getPopulation()
    val maxFit = (pop maxBy (_.fitness)).fitness
    val minFit = (pop minBy (_.fitness)).fitness
    maxFits += maxFit
    minFits += minFit
    val total = (pop map (_.fitness) sum)
    val avg = num.toDouble(total) / pop.length
    avgFits += avg
    totalFits += total
    log("maxFit({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(maxFit))
    log("minFit({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(minFit))
    log("avgFit({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(avg))
    log("totalFit({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(total))
  }
}

object NumericalFitnessGraphCollector {
  def apply[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
           (gpst : GPState[Var, Type, Term, Fit], sltf : StringLoggerTypeFactory)
           (implicit ord : Numeric[Fit]) = 
  {
    val out = new NumericalFitnessGraphCollector[Var, Type, Term, Fit]() with sltf.InfoLogger
    gpst.addCollector(out)
    out
  }
}

abstract class GPStatisticsCollector[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <:Terms[Var, Type, Term], Fit]
(implicit ord : Ordering[Fit]) extends Collector[Var, Type, Term, Fit]
{
  val avgDepths = ListBuffer[Double]()
  val avgSizes = ListBuffer[Double]()
  val bestDepths = ListBuffer[Int]()
  val bestSizes = ListBuffer[Int]()
  
    //tells us who the best fitness was at each step
  override def collectBeforeStep(state : GPState[Var, Type, Term, Fit]) = {
    val pop = state.getPopulation()
    val depth = (pop maxBy (_.fitness)).term.maxDepth
    val size = (pop maxBy (_.fitness)).term.size
    val avgDepth = (pop map (_.term.maxDepth) sum) / pop.size
    val avgSize = (pop map (_.term.size) sum) / pop.size
    avgDepths += avgDepth
    avgSizes += avgSize
    bestDepths += depth
    bestSizes += size
    log("bestDepth({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(depth))
    log("bestSize({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(size))
    log("avgDepth({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(avgDepth))
    log("avgSize({}): {}", state.getGeneration() : java.lang.Integer, new LazyStringable(avgSize))
  }
}

object GPStatisticsCollector {
  def apply[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
           (gpst : GPState[Var, Type, Term, Fit], sltf : StringLoggerTypeFactory)
           (implicit ord : Ordering[Fit]) = 
  {
    val out = new GPStatisticsCollector[Var, Type, Term, Fit]() with sltf.InfoLogger
    gpst.addCollector(out)
    out
  }
}