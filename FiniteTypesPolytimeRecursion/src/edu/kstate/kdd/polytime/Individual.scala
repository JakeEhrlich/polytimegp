package edu.kstate.kdd.polytime

case class Individual[Term, Fit](val term : Term, getFit : Term => Fit) {
  lazy val fitness = getFit(term)
}