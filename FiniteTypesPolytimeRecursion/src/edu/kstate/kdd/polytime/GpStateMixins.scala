package edu.kstate.kdd.polytime

import scala.util.Random
import ATAPLLinear._
import ATAPLLinear.ATAPLLinear._

trait HasLibrary[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit] extends GPState[Var, Type, Term, Fit] {
  val lib : List[UnifiableForm[Var, Type, Term]]
}

trait HasGenerator[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit] extends HasLibrary[Var, Type, Term, Fit] {
  //be carful here: 'gen' must be lazy so that that 'lib' is initlized before 'gen'
  //rule of thumb: if a value is inintlized with a polymorphic member make the member lazy
  lazy val gen : Generator[Var, Type, Term] = new Generator[Var, Type, Term](lib)
}

trait HasAffineLambdaCalculus[Fit] extends HasLibrary[Var, Type, Term, Fit] with HasGenerator[Var, Type, Term, Fit] {
  override val lib = List(
    TrueUnifiableForm,
    FalseUnifiableForm,
    NilUnifiableForm,
    SplitUnifiableForm,
    ComposeUnifiableForm,
    FlipUnifiableForm,
    MakePairUnifiableForm,
    ConstUnifiableForm,
    IterUnifiableForm,
    IfPairUnifiableForm,
    ConsUnifiableForm,
    PairUnifiableForm,
    AppUnifiableForm
  )
}

abstract class HasTermGen[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit](tipe : Type, depths : Seq[Int], widths : Seq[Int])
extends HasGenerator[Var, Type, Term, Fit]
{
  override def genInitTerm(): Term = {
    val rand = new Random()
    while(true) {
      //choose a depth between 8 and 4
      val depth = gen.select(depths)
      //choose a width between 16 and 5
      val width = gen.select(widths)
      gen.generate(tipe, depth, width) match {
        case Some(term) => return term
        case None       => 
      }
    }
    throw new Exception("how did we get here?")
  }
}

abstract class BinomialMutatator[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(pipe : Breeder[Term, Fit], prob : Double, gen : Generator[Var, Type, Term], maxDepth: Int, maxWidth: Int, p : Double) 
extends Mutatator[Var, Type, Term, Fit](pipe, prob, gen, maxDepth, maxWidth) {
  
  private def choose(n : Int, k : Int) = ((k + 1) to n).product / (1 to (n - k)).product
  
  private def binomialDistro(depth : Int): Double = {
    val binom = choose(maxDepth, depth) //correct for array useage
    val p1 = Math.pow(p, depth)
    val p2 = Math.pow(1 - p, maxDepth - depth) //correct for array useage
    binom * p1 * p2
  }
  
  //memoize the distrobution in an array for fast recall
  private val values = Array.tabulate(maxDepth + 1)(binomialDistro)
  
  def mutateHereDistro(d : Int): Double = values(d)
}

abstract class UniformMutatator[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term], Fit]
(pipe : Breeder[Term, Fit], prob : Double, gen : Generator[Var, Type, Term], maxDepth: Int, maxWidth: Int) 
extends Mutatator[Var, Type, Term, Fit](pipe, prob, gen, maxDepth, maxWidth) {
  
  override def mutateHereDistro(depth : Int) = 1.0 / maxDepth.toDouble
}