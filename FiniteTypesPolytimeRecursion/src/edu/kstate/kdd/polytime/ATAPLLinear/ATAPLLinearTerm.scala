package edu.kstate.kdd.polytime.ATAPLLinear

import edu.kstate.kdd.polytime._

//as a helper we define an intermedeite type for evaluation
sealed abstract class Semantics {
  val pretty : String
  def apply(arg : Semantics) = this match {
    case LamEval(lam) => lam(arg)
    case otherwise => {
      //we have an issue
      null
    }
  }
}
case class LamEval(lam : Semantics => Semantics) extends Semantics {
  val pretty = if(meta != null) "lam" + varName + "." + meta else "lam x. <...>"
  var meta : String = null
  var varName : String = null
}
case class PairEval(t1 : Semantics, t2 : Semantics) extends Semantics {
  val pretty = "(" + t1.pretty + ", " + t2.pretty + ")"
}
//thunks do not need to be updated however becuase of the affine nature
//of this calculus
//case class Thunk(t : Term) extends Semantics {
  //val preety = "T<" + t.pretty + ">"
//}
case class ListEval(lst : List[Semantics]) extends Semantics {
  val pretty = lst map (_.pretty) toString
}
/*case class ConsEval(r : RecEval, x : Semantics, xs : Semantics) extends ListEval {
  val preety = "(" + x.preety + " : " + xs.preety + ")"
}
case object NilEval extends ListEval {
  val preety = "[]"
}*/
case class BoolEval(tv : Boolean) extends Semantics {
  val pretty = tv.toString()
}
case class RecEval() extends Semantics {
  val pretty = "_rec_"
}
object RecVal extends RecEval

object Term {

  val splitTerm = LamEval(p => LamEval(f => {
    p match {
      case PairEval(a, b) => f(a)(b)
      case otherwise => {
        //bad stuff
        null
      }
    }
  }))
  
  val flipTerm = LamEval(f => LamEval(x => LamEval(y => {
    f(y)(x)
  })))
  
  val composeTerm = LamEval(f => LamEval(g => LamEval(x => {
    f(g(x))
  })))
  
  val constTerm = LamEval(x => LamEval(y => x))
  
  val ifTerm = LamEval(c => LamEval(x => LamEval(y => {
    c match {
      case BoolEval(tv) => if(tv) x else y
      case otherwise => {
        //we have an issue
        null
      }
    }
    if(c.asInstanceOf[BoolEval].tv) x else y
  })))
  
  val mkPairTerm = LamEval(x => LamEval(y => {
    PairEval(x, y)
  }))
  
  def iter[A, B](ac : B, f : A => (B => B), lst : List[A]): B = lst match {
    case (x :: xs) => {
      val rest = iter(ac, f, xs)
      f(x)(rest)
    }
    case List()    => ac
  }
  
  val iterTerm = LamEval(s => LamEval(rec => LamEval(l => {
    l match {
      case ListEval(lst) => iter(s, (a : Semantics) => (b : Semantics) => rec(a)(b)(RecVal), lst)
      case otherwise => {
        //we have an issue
        null
      }
    }
    
  })))
  
  val consTerm = LamEval(r => LamEval(x => LamEval(xs => {
    xs match {
      case ListEval(lst) => ListEval(x :: lst)
      case othwerise => {
        //bad stuff
        null
      }
    }
  })))
  
  val nilTerm = ListEval(List())
}
//These terms are actully all Affine
sealed abstract class Term  extends Terms[Var, Type, Term] {
 
  override def toString() = pretty
  val pretty : String
  val prec : Int
  
  def evalArgs(args: Term*): Semantics = {
    val tm = args.foldLeft(this)(App(_, _))
    tm.eval()
  }
  
  def evalSemantics(args: Semantics*): Semantics = {
    args.foldLeft(this.eval()){_(_)}
  }
  
  //this defines how these things get evaluated
  def eval(): Semantics
  //these are the most common things needed
  def apply(sub : Map[Var, Type]): Term
  val size = 1
  val maxDepth = 1
  val width = 1
  val getParts = List[TermHole[Var, Type, Term]]()
}

//for our affine lambda calculus we need a special recursion token
case object RecToken extends Term {
  val getType = RecTokenType
  val pretty = "_rec_"
  def eval() = RecVal
  override def apply(sub : Map[Var, Type]) = this
  val preety = "rec"
  val prec = 0
}

//These things are the data that we work with
case class Bool(tv : Boolean) extends Term {
  val getType = BoolType
  val pretty = tv.toString()
  def eval() = BoolEval(tv)
  override def apply(sub : Map[Var, Type]) = this
  val preety = tv.toString()
  val prec = 0
}
case class Pair(t1 : Term, t2 : Term) extends Term {
  val getType = PairType(t1.getType, t2.getType)
  override val size = t1.size + t2.size
  override val maxDepth = t1.maxDepth max t2.maxDepth
  override val getParts = {
    //form2 has the first argument filled in and form1 has the second filled in
    val form2 = FillArgumentForm(PairUnifiableForm, 0, t1)
    val form1 = FillArgumentForm(PairUnifiableForm, 1, t2)
    List(TermHole(getType, form1, t1), TermHole(getType, form2, t2))
  }
  override val width = t1.width + t2.width
  val pretty = "(" + t1.pretty + ", " + t2.pretty + ")"
  def eval() = PairEval(t1.eval(), t2.eval())
  override def apply(sub : Map[Var, Type]) = Pair(t1(sub), t2(sub))
  val prec = 0
}
case class Nil(tipe : Type) extends Term {
  val getType = ListType(tipe)
  override def apply(sub : Map[Var, Type]): Term = Nil(tipe(sub))
  val pretty = "[]"
  def eval() = Term.nilTerm
  val prec = 0
}
case class Cons(tipe : Type) extends Term {
  val getType = FuncType(RecTokenType, FuncType(tipe, FuncType(ListType(tipe), ListType(tipe))))
  override def apply(sub : Map[Var, Type]): Term = Cons(tipe(sub))
  val pretty = "cons"
  def eval() = Term.consTerm
  val preety = "cons"
  val prec = 0
}
object AppUtils {
  val unificator = new Unificator[Var, Type]()
}
//(lam x. E2) E1 = E2(E1) 
case class App(var t1 : Term, var t2 : Term) extends Term {
  import AppUtils._
  
  {
    //(([bool] -> rec -> [bool]) -> [bool] -> rec -> [bool])
    //[bool] -> rec -> [bool]
    
    t1.getType match {
      case FuncType(a, b) if a != t2.getType => {
        unificator.unify(a, t2.getType) match {
          case Left(sub) => {
            t1 = t1(sub)
            t2 = t2(sub)
          }
          case Right(err) => {
            System.out.println(err)
            throw new Exception("this type dosn't even unify!?")
          }
        }
      }
      case FuncType(a, b) => {/*good to go if we got here*/}
      case otherwise => {
        throw new Exception("this isn't even a function type")
      }
    }
  }
  
  val getType = (t1.getType : @unchecked) match {
    case FuncType(t, tt) => tt
    case otherwise => {
      null
    }
  }
  
  private def putParens(t : Term): String = if(t.prec < prec) { t.pretty } else { "(" ++ t.pretty ++ ")"}
  val prec = 3
  val pretty = t1.pretty + " " + putParens(t2)
  
  
  override def apply(sub : Map[Var, Type]): Term = App(t1(sub), t2(sub))
  override val size = t1.size + t2.size
  override val maxDepth = t1.maxDepth max t2.maxDepth
  override val getParts = {
    //form2 has the first argument filled in and form1 has the second filled in
    val form2 = FillArgumentForm(AppUnifiableForm, 0, t1)
    val form1 = FillArgumentForm(AppUnifiableForm, 1, t2)
    List(TermHole(getType, form1, t1), TermHole(getType, form2, t2))
  }
  override val width = t1.width + t2.width
  def eval() = {
    val e1 = t1.eval()
    val e2 = t2.eval()
    e1(e2)
  }
  //val preety = "(" ++ t1.preety ++ " " ++ t2.preety ++ ")"
}
//Split f <x, y> = f x y
case class Split(p1 : Type, p2 : Type, res : Type) extends Term {
  private val fType = FuncType(p1, FuncType(p2, res))
  val getType = FuncType(PairType(p1, p2), FuncType(fType, res))
  override def apply(sub : Map[Var, Type]): Term = Split(p1(sub), p2(sub), res(sub))
  override val pretty = "split"
  def eval() = Term.splitTerm
  val prec = 0
}
//SLeft x y z = x z y  which is just the flip combinator
case class Flip(f : Type, s : Type, res : Type) extends Term {
  private val fType = FuncType(s, FuncType(f, res))
  val getType = FuncType(fType, FuncType(f, FuncType(s, res)))
  override def apply(sub : Map[Var, Type]): Term = Flip(f(sub), s(sub), res(sub))
  override val pretty = "flip"
  def eval() = Term.flipTerm
  val prec = 0
}
//SRight x y z = x (y z) which is just the compostion combinator
case class Compose(a : Type, b : Type, c : Type) extends Term {
  private val fType = FuncType(b, c)
  private val gType = FuncType(a, b)
  val getType = FuncType(fType, FuncType(gType, FuncType(a, c)))
  override def apply(sub : Map[Var, Type]): Term = Compose(a(sub), b(sub), c(sub))
  override val pretty = "comp"
  def eval() = Term.composeTerm
  val prec = 0
}

//Const x y = x 
case class Const(a : Type, b : Type) extends Term {
  val getType = FuncType(a, FuncType(b, a))
  override def apply(sub : Map[Var, Type]): Term = Const(a(sub), b(sub))
  override val pretty = "const"
  def eval() = Term.constTerm
  val prec = 0
}
//Iter c f Nil       = c
//Iter c f (x :: xs) = f x (Iter c f xs) <new R>
case class Iter(a : Type, res : Type) extends Term {
  val sType = res
  val rType = FuncType(a, FuncType(res, FuncType(RecTokenType, res)))
  val getType = FuncType(sType, FuncType(rType, FuncType(ListType(a), res)))
  override def apply(sub : Map[Var, Type]): Term = Iter(a(sub), res(sub))
  override val pretty = "iter"
  def eval() = Term.iterTerm
  val prec = 0
}
//MakePair x y = <x, y>
case class MakePair(a : Type, b : Type) extends Term {
  val getType = FuncType(a, FuncType(b, PairType(a, b)))
  override def apply(sub : Map[Var, Type]): Term = MakePair(a(sub), b(sub))
  override val pretty = "mkpair"
  override def eval() = Term.mkPairTerm
  val prec = 0
}
//If true  x y = x
//If false x y = y
case class If(a : Type) extends Term {
  val getType = FuncType(BoolType, FuncType(a, FuncType(a, a)))
  override def apply(sub : Map[Var, Type]): Term = If(a(sub))
  override val pretty = "if"
  def eval() = Term.ifTerm
  val prec = 0
}