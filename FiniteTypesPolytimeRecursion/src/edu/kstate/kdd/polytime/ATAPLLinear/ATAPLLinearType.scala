package edu.kstate.kdd.polytime.ATAPLLinear

import edu.kstate.kdd.polytime._

sealed abstract class Type extends Types[Var, Type] with Unifiable[Var, Type] {
  def freeVars(): Set[Var] = this match {
    case BoolType => Set()
    case PairType(t1, t2) => t1.freeVars() union t2.freeVars()
    case FuncType(t1, t2) => t1.freeVars() union t2.freeVars()
    case ListType(t) => t.freeVars()
    case RecTokenType => Set()
    case (v : Var) => Set(v)
  }
  
  def apply(sub : Map[Var, Type]): Type = this match {
    case BoolType => this
    case PairType(t1, t2) => PairType(t1(sub), t2(sub))
    case FuncType(t1, t2) => FuncType(t1(sub), t2(sub))
    case ListType(t) => ListType(t(sub))
    case RecTokenType => RecTokenType
    case (v : Var) => if(sub.contains(v)) {
      sub(v)
    } else {
      v
    }
  }
  val preety : String
  val prec : Int
  override def toString() = preety
  def to(tipe : Type) = FuncType(this, tipe)
}
case object RecTokenType extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = x match {
    case RecTokenType => Some(List())
    case _            => None 
  }
  def matchVar(x : Type): Option[Map[Var, Type]] = None
  override val preety = "rec"
  val prec = 0
}
case object BoolType extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = x match {
    case BoolType => Some(List())
    case _        => None 
  }
  def matchVar(x : Type): Option[Map[Var, Type]] = None
  override val preety = "bool"
  val prec = 0
}
case class ListType(t : Type) extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = x match {
    case ListType(tt) => Some(List((t, tt)))
    case _            => None 
  }
  def matchVar(x : Type): Option[Map[Var, Type]] = None
  override val preety = "[" ++ t.preety ++ "]"
  val prec = 0
}
case class PairType(t1 : Type, t2 : Type) extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = x match {
    case PairType(tt1, tt2) => Some(List((t1, tt1), (t2, tt2)))
    case _                  => None 
  }
  def matchVar(x : Type): Option[Map[Var, Type]] = None
  override val preety = "(" ++ t1.preety ++ ", " ++ t2.preety ++ ")"
  val prec = 0
}
case class FuncType(t1 : Type, t2 : Type) extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = x match {
    case FuncType(tt1, tt2) => Some(List((t1, tt1), (t2, tt2)))
    case _        => None 
  }
  def matchVar(x : Type): Option[Map[Var, Type]] = None
  val prec = 5
  private def putParens(t : Type): String = if(t.prec < prec) { t.preety } else { "(" ++ t1.preety ++ ")"}
  override val preety = putParens(t1) ++ " -> " ++ t2.preety
} 
case class Var(v : String) extends Type {
  def matchesTop(x : Type): Option[List[(Type, Type)]] = Some(List())
  def matchVar(x : Type): Option[Map[Var, Type]] = Some(Map(this -> x))
  override val preety = v
  val prec = 0
}
object Var {
  private val names = List("a", "b", "c", "d", "e", "t")
  private var iters = 0
  def getFreshVar(): Var = {
    val base = names(iters % names.length)
    iters += 1
    (iters - 1) / names.length match {
      case 0 => Var(base)
      case 1 => Var(base + "'")
      case 2 => Var(base + base)
      case x => Var(base + (x - 1))
    }
  }
}