package edu.kstate.kdd.polytime.ATAPLLinear

import edu.kstate.kdd.polytime._

class UltraSimpleUnifiableForm(x : Term) extends UnifiableForm[Var, Type, Term] {
  override val numArgs = 0
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    if(desiredType == x.getType) return Some(List()) else None
  }
  def makeTerm(desiredType : Type, args : List[Term]): Term = x
}

class SimpleUnifiableForm(example : => Term) extends UnifiableForm[Var, Type, Term] {
  import ATAPLLinear._
  val cachedExample = example
  override val numArgs = 0
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    unify(cachedExample.getType, desiredType) match {
      case Left(sub) => Some(List())
      case Right(err) => None
    }
  }
  def makeTerm(desiredType : Type, args : List[Term]): Term = {
    val ex = example
    (unify(desiredType, ex.getType) : @unchecked) match {
      case Left(sub) => ex(sub)
    }
  }
}

case object ATAPLLinear {
  def unify(a : Type, b : Type): Either[Map[Var, Type], UnificationError[Type]] = {
    new Unificator[Var, Type]().unify(a, b)
  }
  
  case object TrueUnifiableForm extends UltraSimpleUnifiableForm(Bool(true))
  case object FalseUnifiableForm extends UltraSimpleUnifiableForm(Bool(false))
  case object NilUnifiableForm extends SimpleUnifiableForm(Nil(Var.getFreshVar()))
  case object SplitUnifiableForm extends SimpleUnifiableForm(Split(Var.getFreshVar(), Var.getFreshVar(), Var.getFreshVar()))
  case object ComposeUnifiableForm extends SimpleUnifiableForm(Compose(Var.getFreshVar(), Var.getFreshVar(), Var.getFreshVar()))
  case object FlipUnifiableForm extends SimpleUnifiableForm(Flip(Var.getFreshVar(), Var.getFreshVar(), Var.getFreshVar()))
  case object MakePairUnifiableForm extends SimpleUnifiableForm(MakePair(Var.getFreshVar(), Var.getFreshVar()))
  case object ConstUnifiableForm extends SimpleUnifiableForm(Const(Var.getFreshVar(), Var.getFreshVar()))
  case object IterUnifiableForm extends SimpleUnifiableForm(Iter(Var.getFreshVar(), Var.getFreshVar()))
  case object IfPairUnifiableForm extends SimpleUnifiableForm(If(Var.getFreshVar()))
  case object ConsUnifiableForm extends SimpleUnifiableForm(Cons(Var.getFreshVar()))
}

case object PairUnifiableForm extends UnifiableForm[Var, Type, Term] {
  override val numArgs = 2
  def neededTermTypes(desiredType : Type): Option[List[Type]] = desiredType match {
    case PairType(t1, t2) => Some(List(t1, t2)) //we don't actully have to unify anything for pairs
    case _ => None
  }
  def makeTerm(desiredType : Type, args : List[Term]): Term = Pair(args(0), args(1))
}

case object AppUnifiableForm extends UnifiableForm[Var, Type, Term] {
  override val numArgs = 2
  val a = Var.getFreshVar()
  val b = Var.getFreshVar()
  val fType = FuncType(a, b)
  val xType = a
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = {
    val a = Var.getFreshVar()
    Some(List(FuncType(a, desiredType), a))
  }
  def makeTerm(desiredType : Type, args : List[Term]): Term = App(args(0), args(1))
}

//used for dynamically making new forms for corssover and mutation
case class FillArgumentForm(form : UnifiableForm[Var, Type, Term], argNum : Int, arg : Term) extends UnifiableForm[Var, Type, Term] {
  import ATAPLLinear._
  
  val numArgs = form.numArgs - 1
  
  private def insertAt(in : List[Term], item : Term, at : Int): List[Term] = {
    in.take(at) ++ List(item) ++ in.drop(at)
  }
  
  def neededTermTypes(desiredType : Type): Option[List[Type]] = form.neededTermTypes(desiredType) flatMap { lst =>
    (unify(arg.getType, lst(argNum))).left.toOption map { sub =>
      val (start, _ :: end) = lst map ((ty : Type) => ty(sub)) splitAt argNum
      start ++ end
    }
  }
  
  def makeTerm(desiredType : Type, args : List[Term]): Term = form.makeTerm(desiredType, insertAt(args, arg, argNum))
}
