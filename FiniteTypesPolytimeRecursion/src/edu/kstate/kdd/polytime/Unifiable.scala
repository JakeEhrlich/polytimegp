package edu.kstate.kdd.polytime

//things of types have free varibles and can be substituted into
trait Types[Var, Type] {
  def freeVars(): Set[Var]
  def apply(sub : Map[Var, Type]): Type
}

case class TermHole[Var, Type, Term]
(val tipe : Type, val adam : UnifiableForm[Var, Type, Term], val rib : Term) { 
  def eve(newRib : Term) = adam.makeTerm(tipe, List(newRib))
}

trait Terms[Var, Type, Term <: Terms[Var, Type, Term]] {
  val getType: Type
  val size: Int
  val maxDepth: Int
  val width: Int
  val getParts: List[TermHole[Var, Type, Term]]
  def apply(sub : Map[Var, Type]): Term
}

trait UnifiableForm[Var, Type, Term] {
  //bounds types may be instansiated with anything and thus should always unifiy
  //output should be None if system dose not unify
  //if system does unify then the types of arguments needed to make it unify should be given
  //any hing in the output that can be instansiated with anything should go in the output schema's bound varibles
  def neededTermTypes(desiredType : Type): Option[List[Type]]
  def makeTerm(desiredType : Type, args : List[Term]): Term
  def canMake(desiredType : Type): Boolean = {
    neededTermTypes(desiredType) match {
      case Some(x) => true
      case _       => false
    }
  }
  val numArgs: Int
}

abstract class UnificationError[Type] {}
class UnableToUnify[Type](val a : Type, val b : Type) extends UnificationError[Type] {
  override def toString(): String = s"unable to unify $a and $b"
}
class UnableToUnifyIn[Type](val child : UnificationError[Type], val a : Type, val b : Type) extends UnificationError[Type] {
  override def toString(): String = s"$child \n\t when unifiying $a and $b"
}
class OccursCheck[Var, Type](val v : Var, val t : Type) extends UnificationError[Type] {
  override def toString(): String = s"occurs check fails on $v in $t"
}

//anything which has varibles in it can implement this interface
trait Unifiable[Var, Type <: Unifiable[Var, Type]] {
  //returns Some if the structure mataches
  //contents of some is list of things that must be further unified
  def matchesTop(x : Type): Option[List[(Type, Type)]]
  //returns None if this is not a varible
  //returns the substuatution if it is
  def matchVar(x : Type): Option[Map[Var, Type]]
}


object Unificator {
  //combines two substitutions in correct mannar
  private def composeSub[Var, Type <: Types[Var, Type]](a : Map[Var, Type], b : Map[Var, Type]): Map[Var, Type] = {
    a.mapValues {(x : Type) => x(b)} ++ b
  }
}

//based on https://github.com/wh5a/Algorithm-W-Step-By-Step
//by Martin Grabmuller
//performs unification to find the most general unifier
class Unificator[Var, Type <: Unifiable[Var, Type] with Types[Var, Type]] {
  //preforms the occurs check on a series of substitutions
  //already checked substs must be subtituted into unchecked ones before checking
  //this ensures that things that return multipule unchecked substs work properly
  private def occursCheck(sub : Map[Var, Type]): Either[Map[Var, Type], UnificationError[Type]] = {
    val init : Either[Map[Var, Type], UnificationError[Type]] = Left(Map[Var, Type]())
    def foldAndSub(s : Map[Var, Type]): Either[Map[Var, Type], UnificationError[Type]] = {
      if(s.isEmpty) return init
      foldAndSub(s.tail) match {
        case Left(subst) => {
          val v = s.head._1
          val t = s.head._2(subst)
          if(t != v && t.freeVars()(v)) {
            Right(new OccursCheck[Var, Type](v, t))
          } else {
            Left(subst + (v -> t))
          }
        }
        case Right(msg) => Right(msg)
      }
    }
    foldAndSub(sub)
  }
  
  //preforms unification
  def unify(a : Type, b : Type): Either[Map[Var, Type], UnificationError[Type]] = {
    a.matchVar(b) match {
      case Some(v) => return occursCheck(v)
      case _ =>
    }
    b.matchVar(a) match {
      case Some(v) => return occursCheck(v)
      case _ =>
    }
    a.matchesTop(b) match {
      case Some(matchings) => {
        var out : Either[Map[Var, Type], UnificationError[Type]] = Left(Map())
        for((x, y) <- matchings) {
          out.left.foreach {sub =>
            new Unificator[Var, Type]().unify(x(sub), y(sub)) match {
              case Left(m) => {
                out = Left(Unificator.composeSub(sub, m))
              }
              case Right(msg) => {
                out = Right(new UnableToUnifyIn(msg, a, b))
              }
            }
          }
        }
        return out
      }
      case _ => Right(new UnableToUnify(a, b))
    }
  }
}

