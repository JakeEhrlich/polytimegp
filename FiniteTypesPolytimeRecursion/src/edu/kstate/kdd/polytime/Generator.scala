package edu.kstate.kdd.polytime

import java.util.concurrent.ThreadLocalRandom
import scala.util.Random

//TODO: add in probability distro over library units
class Generator[Var, Type <: Unifiable[Var, Type] with Types[Var, Type], Term <: Terms[Var, Type, Term]]
(library : List[UnifiableForm[Var, Type, Term]]) 
{
  
  def select[T](items : Seq[T]): T = items(ThreadLocalRandom.current().nextInt(items.length))
  
  def tryNTimes[T](n : Int)(f : => Option[T]): Option[T] = {
    for(i <- 0 to 3) {
      f match {
        case None => {}
        case Some(x) => return Some(x)
      }
    }
    None
  }
  
  def shuffle[T](lst : List[T]) = {
    val rand = new Random(ThreadLocalRandom.current())
    rand.shuffle(lst)
  }
  
  //generates a random term if possible
  def generate(tipe : Type, depth : Int, width : Int): Option[Term] = {
    //find the usable items
    val usable = library.filter(p => p.canMake(tipe))
    //if there are non we have failed
    if(usable.length == 0) return None
    //if we need exactly 1 item
    if(depth == 1) {
      //find only the terms that don't need any other arguments
      val finiteUsable = usable.filter(p => p.numArgs == 0)
      //again if there are none we have failed
      if(finiteUsable.length == 0) {
        return None
      }
      //otherwise select one of them and return the term
      val chosen = select(finiteUsable)
      return Some(chosen.makeTerm(tipe, List()))
    }
    //look though all the items that are non-finite so we can generate an item of the requested depth
    val nonFinite = usable.filter(p => {
      p.numArgs > 0 && p.numArgs < width
    })
    //if there are none then just return a shorter one if possible
    if(nonFinite.length == 0) return {generate(tipe, depth - 1, width)}
    val chosen = select(nonFinite) //select one if it exists
    val neededTerms = chosen.neededTermTypes(tipe) //find what we need to give the chosen form
    //try and fill out those forms
    val out = neededTerms flatMap { items =>
      //try 3 times to get a good result otherwise, fuck it
      val optArgs = tryNTimes[List[(Int, Term)]](3) {
        var neededTermTypes = items
        var remainingWidth = width
        val unificator = new Unificator[Var, Type]()
        //fold though each item changing neededTerms based on already chosen values
        shuffle((0 until items.length).toList).foldRight[Option[List[(Int, Term)]]](Some(List())) { (next, acc) =>
          acc flatMap { x =>
            val res = generate(neededTermTypes(next), depth - 1, remainingWidth)
            val out = res flatMap { tm => {
                unificator.unify(tm.getType, neededTermTypes(next)).left.toOption map { sub =>
                  //now that we know we had a good result change things
                  //this mutation stuff is so fucked up and convoluted though
                  res map {term =>
                    remainingWidth = remainingWidth - term.width  
                  }
                  neededTermTypes = neededTermTypes map (x => x(sub))
                  //also make sure to update the old stuff with any new substitutions
                  (next, tm) :: (x map {value =>
                    val (idx, oldTm) = value
                    (idx, oldTm(sub))
                  })
                }
              }
            }
            out
          }
        }
      }
      optArgs map { args => {
        val sortedArgs = args.sortBy(_._1) map {_._2}
        chosen.makeTerm(tipe, sortedArgs) }
      }
    }
    //finally if we couldn't make it work at this depth, try a smaller depth
    if(out.isEmpty) generate(tipe, depth - 1, width)
    else out
  }
}