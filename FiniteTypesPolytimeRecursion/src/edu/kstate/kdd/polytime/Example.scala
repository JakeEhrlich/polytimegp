package edu.kstate.kdd.polytime

import ATAPLLinear._
import ATAPLLinear.ATAPLLinear._
import scala.util.Random

import ch.qos.logback.classic.Logger
import ch.qos.logback.core.FileAppender
import org.slf4j.LoggerFactory


//private object tipe extends FuncType(BoolType, FuncType(BoolType, FuncType(BoolType, BoolType)))

object GpExample {
  
  val tipe = BoolType to (BoolType to (BoolType to BoolType))
  println(tipe)
  val gpstate = new HasTermGen[Var, Type, Term, Double](tipe, 3 to 7, 4 to 9)
                with HasAffineLambdaCalculus[Double]
  {
    //define the breeding pipeline
    private object randSel extends RandomSelection[Term, Double]()
    private object selector extends TournamentSelection[Term, Double](randSel, 25)
    override object breeder extends BinomialMutatator[Var, Type, Term, Double](selector, 0.2, gen, 7, 9, 0.1)
      
    //specify the name of the logger so that the config file can be used
    private val sltf = new StringLoggerTypeFactory(LoggerFactory.getLogger("root"))
    //define the information you want to collect
    private val tc = TimeCollector(this, sltf)
    private val btc = BestTermCollector(this, sltf)
    private val ntc = NumericalFitnessGraphCollector(this, sltf)
    private val gpc = GPStatisticsCollector(this, sltf)
  
    //define the population size
    override val populationSize: Int = 500
    
    //define when you should stop
    override def shouldStop(): Boolean = this.getGeneration() >= 500
    
    //define the fitness function
    override def getFitness(tm: Term): Double = {
      val t = Bool(true)
      val f = Bool(false)
      val out = for(a <- List(t, f); b <- List(t, f); c <- List(t, f)) yield {
        tm.evalArgs(a, b, c) match {
          case BoolEval(true) => if((a != b) != c.tv) 1 else 0
          case BoolEval(false) => if((a != b) != c.tv) 0 else 1
          case otherwise    => {
            -1000 //how did this happen?
          }
        }
      }
      out sum
    }
  }
  
  def run() {
    gpstate.run()
  }
} 
                     