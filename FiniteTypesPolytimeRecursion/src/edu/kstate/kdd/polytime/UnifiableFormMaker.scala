package edu.kstate.kdd.polytime


trait Blarg[Var, Type, Term] {
  //bounds types may be instansiated with anything and thus should always unifiy
  //output should be None if system dose not unify
  //if system does unify then the types of arguments needed to make it unify should be given
  //any hing in the output that can be instansiated with anything should go in the output schema's bound varibles
  def neededTermTypes(desiredType : Type): Option[List[Type]]
  def makeTerm(desiredType : Type, args : List[Term]): Term
  def canMake(desiredType : Type): Boolean = {
    neededTermTypes(desiredType) match {
      case Some(x) => true
      case _       => false
    }
  }
}


class UnifiableFormMaker {

}