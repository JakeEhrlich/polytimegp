package edu.kstate.kdd.polytime

import edu.kstate.kdd.polytime.ATAPLLinear._
import edu.kstate.kdd.polytime.ATAPLLinear.ATAPLLinear._

object GeneratorTests {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(203); 
  println("Welcome to the Scala worksheet");$skip(150); 
  
  val lib = List[UnifiableForm[Var, Type, Term]](ComposeUnifiableForm, FlipUnifiableForm, ConstUnifiableForm, AppUnifiableForm, TrueUnifiableForm);System.out.println("""lib  : List[edu.kstate.kdd.polytime.UnifiableForm[edu.kstate.kdd.polytime.ATAPLLinear.Var,edu.kstate.kdd.polytime.ATAPLLinear.Type,edu.kstate.kdd.polytime.ATAPLLinear.Term]] = """ + $show(lib ));$skip(51); 
  
  val gen = new Generator[Var, Type, Term](lib);System.out.println("""gen  : edu.kstate.kdd.polytime.Generator[edu.kstate.kdd.polytime.ATAPLLinear.Var,edu.kstate.kdd.polytime.ATAPLLinear.Type,edu.kstate.kdd.polytime.ATAPLLinear.Term] = """ + $show(gen ));$skip(44); val res$0 = 
  Const(Var("Float"), Var("Float")).getType;System.out.println("""res0: edu.kstate.kdd.polytime.ATAPLLinear.FuncType = """ + $show(res$0));$skip(57); val res$1 = 
  Flip(Var("Float"), Var("Float"), Var("Float")).getType;System.out.println("""res1: edu.kstate.kdd.polytime.ATAPLLinear.FuncType = """ + $show(res$1));$skip(106); val res$2 = 
  App(App(Flip(BoolType, Var("Float"), Var("Float")), Const(Var("Float"), BoolType)), Bool(true)).getType;System.out.println("""res2: edu.kstate.kdd.polytime.ATAPLLinear.Type = """ + $show(res$2));$skip(148); val res$3 = 
                                                  
                                                
  gen.generate(FuncType(BoolType, BoolType), 3);System.out.println("""res3: Option[edu.kstate.kdd.polytime.ATAPLLinear.Term] = """ + $show(res$3));$skip(91); 
  
  val Some(List(f, x)) = AppUnifiableForm.neededTermTypes(FuncType(BoolType, BoolType));System.out.println("""f  : edu.kstate.kdd.polytime.ATAPLLinear.Type = """ + $show(f ));System.out.println("""x  : edu.kstate.kdd.polytime.ATAPLLinear.Type = """ + $show(x ));$skip(63); 
  val Some(List(ff, xx)) = AppUnifiableForm.neededTermTypes(f);System.out.println("""ff  : edu.kstate.kdd.polytime.ATAPLLinear.Type = """ + $show(ff ));System.out.println("""xx  : edu.kstate.kdd.polytime.ATAPLLinear.Type = """ + $show(xx ));$skip(59); 
  val Some(List()) = FlipUnifiableForm.neededTermTypes(ff);$skip(54); 
  val ffFlip = FlipUnifiableForm.makeTerm(ff, List());System.out.println("""ffFlip  : edu.kstate.kdd.polytime.ATAPLLinear.Term = """ + $show(ffFlip ));$skip(60); 
  val Some(List()) = ConstUnifiableForm.neededTermTypes(xx);$skip(56); 
  val xxConst = ConstUnifiableForm.makeTerm(xx, List());System.out.println("""xxConst  : edu.kstate.kdd.polytime.ATAPLLinear.Term = """ + $show(xxConst ));$skip(65); 
  val fApp = AppUnifiableForm.makeTerm(f, List(ffFlip, xxConst));System.out.println("""fApp  : edu.kstate.kdd.polytime.ATAPLLinear.Term = """ + $show(fApp ));$skip(109); 
                                                  
  val Some(List()) = TrueUnifiableForm.neededTermTypes(x);$skip(52); 
  val xTrue = TrueUnifiableForm.makeTerm(x, List());System.out.println("""xTrue  : edu.kstate.kdd.polytime.ATAPLLinear.Term = """ + $show(xTrue ));$skip(87); 
  val res = AppUnifiableForm.makeTerm(FuncType(BoolType, BoolType), List(fApp, xTrue));System.out.println("""res  : edu.kstate.kdd.polytime.ATAPLLinear.Term = """ + $show(res ));$skip(14); val res$4 = 
  res.getType;System.out.println("""res4: edu.kstate.kdd.polytime.ATAPLLinear.Type = """ + $show(res$4))}
}
